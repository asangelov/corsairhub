document.getElementsByClassName("stream-play")[0].addEventListener("click", stream);


function stream(){
	var x = document.getElementById('stream');
	x.innerHTML = "";
	var source1 = document.createElement("iframe");
	var source2 = document.createElement("iframe");
	var hr = document.createElement("hr");

	source1.setAttribute("src", "https://hls.hdv.fun/imdb/" + this.id);
	source1.setAttribute("class", "moviestream");
	source1.setAttribute("id", "source1");
	source1.setAttribute("height", "600px");
	source1.setAttribute("width", "100%");
	source1.setAttribute("frameborder", "0");
	source1.setAttribute("allowfullscreen", "allowfullscreen");
	source1.setAttribute("scrolling", "no");

	source2.setAttribute("src", "https://database.gdriveplayer.us/player.php?imdb=" + this.id);
	source2.setAttribute("class", "moviestream");
	source2.setAttribute("id", "source2");
	source2.setAttribute("height", "600px");
	source2.setAttribute("width", "100%");
	source2.setAttribute("frameborder", "0");
	source2.setAttribute("allowfullscreen", "allowfullscreen");
	source2.setAttribute("scrolling", "no");

	var text1 = document.createTextNode("Source 1:");
	var text2 = document.createTextNode("Source 2:");

	x.appendChild(text1);
	x.appendChild(source1);
	
	x.appendChild(hr.cloneNode(true));

	x.appendChild(text2);
	x.appendChild(source2);
}

// function fullscr1() {
// 	if (document.getElementsByTagName("iframe")[0].classList.contains("fullScreen")) {
// 		document.getElementsByTagName("iframe")[0].className = "";
// 	}
// 	else {
// 		document.getElementsByTagName("iframe")[0].className = "fullScreen";
// 		document.getElementById("stream").style.position = "static";
// 		document.getElementById("fullscrElement1").style.visibility = "hidden";
// 		document.getElementById("fullscrElement2").style.visibility = "hidden";
// 		// document.getElementById("fullscrElement3").style.visibility = "hidden";
// 		document.getElementById("fullscrElement4").style.visibility = "hidden";

// 	}
// 	var elem = document.body; // Make the body go full screen.
// 	toggleFull(elem);
// }

// function fullscr2() {
// 	if (document.getElementsByTagName("iframe")[1].classList.contains("fullScreen")) {
// 		document.getElementsByTagName("iframe")[1].className = "";
// 	}
// 	else {
// 		document.getElementsByTagName("iframe")[1].className = "fullScreen";
// 		document.getElementById("stream").style.position = "static";
// 		document.getElementById("fullscrElement1").style.visibility = "hidden";
// 		document.getElementById("fullscrElement2").style.visibility = "hidden";
// 		// document.getElementById("fullscrElement3").style.visibility = "hidden";
// 		document.getElementById("fullscrElement4").style.visibility = "hidden";
// 	}
// 	var elem = document.body;
// 	toggleFull(elem);
// }

// // function fullscr3() {
// // 	if (document.getElementsByTagName("iframe")[2].classList.contains("fullScreen")) {
// // 		document.getElementsByTagName("iframe")[2].className = "";
// // 	}
// // 	else {
// // 		document.getElementsByTagName("iframe")[2].className = "fullScreen";
// // 		document.getElementById("stream").style.position = "static";
// // 		document.getElementById("fullscrElement1").style.visibility = "hidden";
// // 		document.getElementById("fullscrElement2").style.visibility = "hidden";
// // 		document.getElementById("fullscrElement3").style.visibility = "hidden";
// // 		document.getElementById("fullscrElement4").style.visibility = "hidden";
// // 	}
// // 	var elem = document.body;
// // 	toggleFull(elem);
// // }

// function fullscr4() {
// 	if (document.getElementsByTagName("iframe")[2].classList.contains("fullScreen")) {
// 		document.getElementsByTagName("iframe")[2].className = "";
// 	}
// 	else {
// 		document.getElementsByTagName("iframe")[2].className = "fullScreen";
// 		document.getElementById("stream").style.position = "static";
// 		document.getElementById("fullscrElement1").style.visibility = "hidden";
// 		document.getElementById("fullscrElement2").style.visibility = "hidden";
// 		// document.getElementById("fullscrElement3").style.visibility = "hidden";
// 		document.getElementById("fullscrElement4").style.visibility = "hidden";
// 	}
// 	var elem = document.body;
// 	toggleFull(elem);
// }

// function cancelFullScreen(el) {
// 	var requestMethod = el.cancelFullScreen || el.webkitCancelFullScreen || el.mozCancelFullScreen || el.exitFullscreen;
// 	if (requestMethod) { // cancel full screen.
// 		requestMethod.call(el);
//  	}
// }

// function requestFullScreen(el) {
// 	// Supports most browsers and their versions.
// 	var requestMethod = el.requestFullScreen || el.webkitRequestFullScreen || el.mozRequestFullScreen || el.msRequestFullscreen;
// 	if (requestMethod) { // Native full screen.
// 		requestMethod.call(el);
// 	}
// }

// function toggleFull() {
// 	var elem = document.body; // Make the body go full screen.
// 	var isInFullScreen = (document.fullScreenElement && document.fullScreenElement !== null) ||  (document.mozFullScreen || document.webkitIsFullScreen);

// 	if (isInFullScreen) {
// 		cancelFullScreen(document);
// 	} else {
// 		requestFullScreen(elem);
// 	}
// 	return false;
// }

// document.addEventListener("fullscreenchange", onFullScreenChange, false);
// document.addEventListener("webkitfullscreenchange", onFullScreenChange, false);
// document.addEventListener("mozfullscreenchange", onFullScreenChange, false);

// function onFullScreenChange() {
// 	var fullscreenElement = document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement;

// 	// if in fullscreen mode fullscreenElement won't be null
// 	if (fullscreenElement == null){
// 		document.getElementsByTagName("iframe")[0].className = "";
// 		document.getElementsByTagName("iframe")[1].className = "";
// 		// document.getElementsByTagName("iframe")[2].className = "";
// 		document.getElementsByTagName("iframe")[2].className = "";
// 		document.getElementById("stream").style.position = "relative";
		
// 		document.getElementById("fullscrElement1").style.visibility = "visible";
// 		document.getElementById("fullscrElement2").style.visibility = "visible";
// 		// document.getElementById("fullscrElement3").style.visibility = "visible";
// 		document.getElementById("fullscrElement4").style.visibility = "visible";

// 	}
// }