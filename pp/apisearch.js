document.getElementById('showall').onclick = function() {
	var query = window.location.search.split('=')[1];
	query = query.replace('%20', ' ');
	query = query.replace('+', ' ');
	if (query.length > 0){
		var request = new XMLHttpRequest();
		request.open("GET", "/web1/apisearch.php?query="+query, true);
		request.setRequestHeader('Access-Control-Allow-Origin','*');
		request.onload = function () {
			document.getElementById('searchapi').innerHTML = this.responseText;
		}
		request.send();
	}
}