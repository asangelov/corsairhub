<?php
	session_start();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>CorsairHub - Login</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="/web1/img/favicon.ico">
		<link rel="stylesheet" href="../style.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	</head>
	<body>
		<header>
			<?php
				require $_SERVER['DOCUMENT_ROOT'].'/web1/header.php';
			?>
		</header>
		<main>
			<?php 
				if (isset($_SESSION['logged'])){
					echo "<h1 style='text-align:center; margin: 350px 0;'>Already logged in!</h1>";
					header('Refresh: 1; url=/index.php');
				}
				else 
				{
					echo 
					"<section id='form'>
						<h1>Login</h1>
						<form action='../login.php' method='post'>
							<p>
								<label for='bsn'>BSN:</label>
								<input type='text' name='bsn' id='bsn'>
							</p>
							<p>
								<label for='password'>Password:</label>
								<input type='password' name='password' id='password'>
							</p>";
							
					if(isset($_COOKIE['error'])) {
						echo "<p><span style='color: red;'>".$_COOKIE['error']."</span></p>";
						setcookie('error', '', time() - 3600, '/');
					}
					echo
							"<p>
								<input type='checkbox' name='rememberme' id='rememberme'>
								<label for='rememberme'>Remember me</label>
								<input type='submit' id='submit' value='Log in'>
							</p>
						</form>
					</section>";
				}
			?>
		</main>
		<footer>
			<?php
				require ($_SERVER['DOCUMENT_ROOT'].'/web1/footer.php');
			?>
		</footer>
		<script src="/web1/ajaxsearch.js"></script>
		<script type="text/javascript" src="/web1/form-validation.js"></script>
	</body>
</html>
