<?php
	session_start();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>CorsairHub - Movie</title>
		<meta name="description" content="">
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="/web1/img/favicon.ico">
		<link rel="stylesheet" type="text/css" href="/web1/style.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	</head>
	<body>
		<header>
			<?php
				require ($_SERVER['DOCUMENT_ROOT'].'/web1/header.php');
			?>
		</header>

		<main>

			<?php
				$id = "";
				if (count(explode('=', $_SERVER['QUERY_STRING'])) > 1){
					$id = explode('=', $_SERVER['QUERY_STRING'])[1];
				}

				$user = 'xx';
				$password = 'xx';

				$con = new PDO('mysql:host=studmysql01.fhict.local;dbname=dbi418108', $user, $password);

				$sql = 'select * from movies where imdbID = ?';

				$statement = $con->prepare($sql);

				$statement->bindParam('1', $id);

				$statement->execute();

				$result = $statement->fetchAll();
				$movieFound = false;

				if (count($result) > 0){
					//movie exists
					$title = $result[0]['title'];
					$hash = $result[0]['hash'];
					$genres = $result[0]['genres'];
					$directors = $result[0]['directors'];
					$cast = $result[0]['actors'];
					$summary = $result[0]['summary'];
					$posterLink = $result[0]['posterLink'];
					$year = explode('-', $result[0]['releaseDate'])[0];
					$movieFound = true;
				}
				else 
				{
					//make an api request to get info
					$hash = "<a href='addmovie.php?id=".$id."'>Add this hash</a>";
					
					function get_http_response_code($url) {
						$headers = get_headers($url);
						return substr($headers[0], 9, 3);
					}


					$uri = 'http://www.omdbapi.com/?i='.$id.'&plot=full&apikey=e49b4e3d';
					$reqPrefs['http']['method'] = 'GET';
					$stream_context = stream_context_create($reqPrefs);
					try {
						$response = file_get_contents($uri, false, $stream_context);
						$data = json_decode($response, true);
						if ($data['Response'] == "True"){
							$title = $data['Title'];
							$genres = $data['Genre'];
							$summary = $data['Plot'];
							$summary = str_replace("'","\\'", $summary);
							$rating = floatval($data['imdbRating']);
							$posterLink = $data['Poster'];
							$year = explode(' ', $data['Released'])[2];
							$title = str_replace("'","\\'", $title);
							
							$directors = $data['Director'];
							$cast = $data['Actors'];
							$movieFound = true;
						}
					}
					catch (Exception $e)
					{
						
					}
					

					$uri = 'https://api.themoviedb.org/3/movie/'.$id.'?api_key=79cbfed63302bc4c9b95acbc95ef1305';
					$reqPrefs['http']['method'] = 'GET';
					$stream_context = stream_context_create($reqPrefs);

					if(get_http_response_code($uri) != "200"){
						
					}
					else
					{
						try {
							$response = file_get_contents($uri, false, $stream_context);
							$data = json_decode($response, true);
							if (!isset($data['status_code'])){
								$year = explode('-', $data['release_date'])[0];
								$rating = floatval($data['vote_average']);
								$posterLink = 'http://image.tmdb.org/t/p/w500'.$data['poster_path'];
								$summary = $data['overview'];
								$summary = str_replace("'","\\'", $summary);
								$movieFound = true;
							}
							else {

							}
						}
						catch (Exception $e){
							
						}
						
					}
					
				}
				
				$stream = "<a href='javascript:;' class='stream-play' id='".$id."'>Try to play</a>";

				if ($movieFound){
					echo "<h1 id='movie-title'>".$title."</h1>";
					echo "<section id='movie'>";
						echo "<table>";
							echo "<tr>";
								echo "<td>Hash</td>";
								echo "<td>".$hash."</td>";
							echo "</tr>";
							echo "<tr>";
								echo "<td>Genres</td>";
								echo "<td>".$genres."</td>";
							echo "</tr>";
							echo "<tr>";
								echo "<td>Year</td>";
								echo "<td>".$year."</td>";
							echo "</tr>";
							echo "<tr>";
								echo "<td>Director</td>";
								echo "<td>".$directors."</td>";
							echo "</tr>";
							echo "<tr>";
								echo "<td>Cast</td>";
								echo "<td>".$cast."</td>";
							echo "</tr>";
							echo "<tr>";
								echo "<td>Summary</td>";
								echo "<td>".$summary."</td>";
							echo "</tr>";
							echo "<tr>";
								echo "<td>Stream</td>";
								echo "<td id='stream'>".$stream."</td>";
							echo "</tr>";
						echo "</table>";
						echo "<img src='".$posterLink."' alt='' class='poster'>";
					echo "</section>";
				}
				else {
					echo "<h1 style='text-align:center; margin: 350px 0;'>Incorrect ID!</h1>";
					header('Refresh: 1; url=index.php');
				}
			?>
		</main>
		<footer>
			<?php
				require ($_SERVER['DOCUMENT_ROOT'].'/web1/footer.php');
			?>
		</footer>
		<script>
			document.title = "CorsairHub - " + document.getElementById('movie-title').innerHTML;
		</script>
		<script src="/web1/ajaxsearch.js"></script>
		<script src="/web1/movie.js"></script>
	</body>
</html>
