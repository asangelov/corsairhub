<?php
	session_start();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>CorsairHub - Contact us</title>
		<link rel="shortcut icon" href="/web1/img/favicon.ico">
		<link rel="stylesheet" href="../style.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	</head>
	<body>
		<header>
			<?php
				require $_SERVER['DOCUMENT_ROOT'].'/web1/header.php';
			?>
		</header>
		<main>
			<section id="form">
				<h1>Get in touch with us</h1>
				<form class="" action="mail.php" method="post">
					<p>
						<label for="name">Your name:</label>
						<input type="text" name="name" id="name">
					</p>
					<p>
						<label for="email">E-mail:</label>
						<input type="email" name="email" id="email">
					</p>
					<p>
						<label for="message">Message:</label>
						<textarea name="message" rows="8" cols="80" id="message"></textarea>
					</p>
					<p>
						<input type="submit" name="send" value="Send"></input>
					</p>
				</form>
			</section>
		</main>
		<footer>
			<?php
				require ($_SERVER['DOCUMENT_ROOT'].'/web1/footer.php');
			?>
		</footer>
		<script src="/web1/ajaxsearch.js"></script>
	</body>
</html>
