<?php
	$to = 'a.angelov@student.fontys.nl';
	$subject = 'Mail from '.$_POST['name'].' /CorsairHub';
	$message = wordwrap($_POST['message'], 70, '\r\n');
	$from = 'From: '.$_POST['email'];

	mail($to, $subject, $message, $from);

	session_start();

	echo "<!DOCTYPE html>
<html lang='en'>
	<head>
		<meta charset='UTF-8'>
		<meta name='viewport' content='width=device-width, initial-scale=1.0'>
		<title>CorsairHub - Contact us</title>
		<link rel='stylesheet' href='../style.css'>
		<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.1/css/all.css' integrity='sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr' crossorigin='anonymous'>
	</head>
	<body>
		<header>";
		require ($_SERVER['DOCUMENT_ROOT'].'/header.php');
		echo "</header>
				<main>
					<h1 style='text-align:center; margin: 350px 0;'>Email send successfully!</h1>
				</main>
				<footer>";
		require ($_SERVER['DOCUMENT_ROOT'].'/footer.php');
		echo "</footer>
			<script src='/ajaxsearch.js'></script>
		</body>
	</html>";


		header('Refresh: 2; url=/contact-us/index.php');
?>