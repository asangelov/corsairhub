<?php
	session_start();
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>CorsairHub - Search</title>
		<meta name="description" content="">
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="/web1/img/favicon.ico">
		<link rel="stylesheet" type="text/css" href="/web1/style.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	</head>
	<body>
		<header>
			<?php
				require ($_SERVER['DOCUMENT_ROOT'].'/web1/header.php');
			?>
		</header>

		<main id="search-page">
			<?php
				$query = "";
				if (count(explode('=', $_SERVER['QUERY_STRING'])) > 1){
					$query = explode('=', $_SERVER['QUERY_STRING'])[1];
					$query = explode('&', $query)[0];
				}
				$query = str_replace('+', ' ', $query);
				$query = str_replace('%20', ' ', $query);
				$query = trim($query);

				if ($query != ""){
					if (substr($query, 0, 2) == 'tt'  && strlen($query) <= 10  && ctype_digit(substr($query, 2))){
						header('location:/web1/movie.php?id='.$query);
					}

					echo "<h1>Search for ".$query."</h1>";

					$user = 'xx';
					$password = 'xx';

					$con = new PDO('mysql:host=studmysql01.fhict.local;dbname=dbi418108', $user, $password);

					$sql = "select * from movies where title like :keyword or actors like :keyword;";

					$statement = $con->prepare($sql);

					$statement->bindValue(':keyword','%'.$query.'%');

					$statement->execute();

					$result = $statement->fetchAll();

					if (count($result) > 0){
						echo "<h2>Available movies:</h2>";
						echo "<section class='movies'>";
						for ($i=0; $i < count($result); $i++) {

							echo "<article>
									<h3>".$result[$i]['title']."</h3>
									<a href='/web1/movie.php?id=".$result[$i]['imdbID']."'><img src='".$result[$i]['posterLink']."' alt='Movie poster of ".$result[$i]['title']."'></a>
									<p>".round($result[$i]['rating']/2,1)."/5</p>
								</article>";
						}
						echo "</section>";
					}
					else 
					{
						echo "<h2>No results found in available movies.</h2>";
						echo "<div id='searchapi'>
							<a href='#' id='showall'>Show unavailable movies</a>
						</div>";
					}
				}
				else {
					header('location:/web1/index.php');
				}

				// $unavailable = "0";
				// if (count(explode('=', $_SERVER['QUERY_STRING'])) > 2){
				// 	$unavailable = explode('=', $_SERVER['QUERY_STRING'])[2];
				// }

				// if ($unavailable = "1"){
				// 	echo $unavailable;
				// 	$path = $_SERVER['DOCUMENT_ROOT'].'/web1/apisearch.php?query='.$query;
				// 	$reqPrefs['http']['method'] = 'GET';
				// 	$stream_context = stream_context_create($reqPrefs);
				// 	$response = file_get_contents($path, true, $stream_context);
				// 	echo $response;
				// 	include ($path);
				// }
				// else {
					// echo "<div id='searchapi'>
					// 		<a href='#' id='showall'>Show unavailable movies</a>
					// 	</div>";
				// }
				
				
			?>
		</main>
		<footer>
			<?php
				require ($_SERVER['DOCUMENT_ROOT'].'/web1/footer.php');
			?>
		</footer>
		<script src="/web1/ajaxsearch.js"></script>
		<script src="/web1/apisearch.js"></script>
	</body>
</html>
