<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, Accept, Authorization, X-Request-With');
	header('Access-Control-Allow-Credentials: true');
	$query = $_GET['query'];

	$user = 'xx';
	$password = 'xx';

	$con = new PDO('mysql:host=studmysql01.fhict.local;dbname=dbi418108', $user, $password);

	$sql = "select * from movies where title like :keyword or actors like :keyword or imdbID = :id order by popularity desc;";

	$statement = $con->prepare($sql);

	$statement->bindValue(':keyword','%'.$query.'%');
	$statement->bindValue(':id', $query);

	$statement->execute();

	$result = $statement->fetchAll();

	$max = 5;	

	if (count($result) > 0){
		//found local movie(s)
		$shownMovies = 0;
		for ($i=0; $i < count($result); $i++) { //loop through all movies
			if ($shownMovies < $max){ //display only the first $max
				$movieName = substr($result[$i]['title'], 0, 30);
				if ($result[$i]['title'] == $movieName){ //check if the movie's title isnt too long and display accordingly
					echo "<article class='result'><a href='/web1/movie.php?id=".$result[$i]['imdbID']."'>".$movieName."</a></article>";
				}
				else {
					echo "<article class='result'><a href='/web1/movie.php?id=".$result[$i]['imdbID']."'>".$movieName."...</a></article>";
				}
				$shownMovies++;
			}
			else {
				echo "<article class='result showall'><a href='/web1/search.php?query=".$query."'>Show all results for ".$query."</a></article>"; //link to full search page if more than $max movies found
				break;
			}
		}
	}
	else {
		//not found in local db
		if (substr($query, 0, 2) == 'tt'  && strlen($query) <= 10 && strlen($query) > 8  && ctype_digit(substr($query, 2))){ //check if the query is an id
			function get_http_response_code($url) { //get response code
				$headers = get_headers($url);
				return substr($headers[0], 9, 3);
			}
			$uri = 'https://api.themoviedb.org/3/movie/'.$query.'?api_key=79cbfed63302bc4c9b95acbc95ef1305';
			$reqPrefs['http']['method'] = 'GET';
			$stream_context = stream_context_create($reqPrefs);

			if(get_http_response_code($uri) != "200"){
				echo "<article class='result showall'><a href=''>Incorrect ID!</a></article>"; // show if a movie with this id does not exist
			}
			else
			{
				try {
					$response = file_get_contents($uri, false, $stream_context); //create and send request
					$data = json_decode($response, true); //make response into json format
					if (!isset($data['status_code'])){ //check if any movie is returned by the API
						$title = $data['title'];
						$rating = floatval($data['vote_average']);
						$posterLink = 'http://image.tmdb.org/t/p/w500'.$data['poster_path'];
						echo "<article class='result'><a href='/web1/movie.php?id=".$query."'>".$title."</a></article>"; //show a link to the movie and its title
					}
				}
				catch (Exception $e){

				}
			}
		}
		else {
			echo "<article class='result showall'><a href='/web1/search.php?query=".$query."&u=1'>Show unavailable movies</a></article>"; //link to show full search page if query is not an id
		}
		
	}

?>
