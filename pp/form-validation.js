//wait for click and call function checkForErrors on click
document.getElementById("submit").addEventListener("click", checkForErrors);
document.getElementById("search-form").addEventListener("submit", checkForEmpty);

function checkForErrors(e){
	//save the bsn and check if its under 11 symbols
	var bsn = document.getElementById("bsn").value;
	if (bsn.length < 11){
		//stops the form from submitting
		e.preventDefault();
		//displays error
		alert("BSN is too short");
		//exits the function
		return false;
	}
}

function checkForEmpty(e){
	var query = document.getElementById("search").value;
	if (query.trim().length == 0){
		//stops the form from submitting
		e.preventDefault();
		//exits the function
		return false;
	}
}